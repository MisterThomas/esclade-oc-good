package com.escalade.oc.escalade.escaladeocsite.model;


import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "site")
public class Site {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private String userName;


    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }


    @Column(name = "cotation")
    private String cotation;


    @Column(name = "longueur")
    private String longueur;

    @Column(name = "ville")
    private String ville;

    @NotNull
    @Column(nullable = false, name = "vaide_site")
    private boolean valideSite;


    @OneToMany(mappedBy = "site", cascade = CascadeType.ALL,
            fetch = FetchType.LAZY)
    private Set<Commentaire> commentaire = new HashSet<>();

    public Set<Commentaire> getCommentaire() {
        return commentaire;
    }

    public void setCommentaire(Set<Commentaire> commentaire) {
        this.commentaire = commentaire;
    }


    @OneToMany(mappedBy = "site", cascade = CascadeType.ALL,
            fetch = FetchType.LAZY)
    private Set<Topo> topo = new HashSet<>();

    public Set<Topo> getTopo() {
        return topo;
    }

    public void setTopo(Set<Topo> topo) {
        this.topo = topo;
    }

    public Site() {

    }


    public Site(String user, String cotation, String longueur, String ville, boolean valideSite, boolean isDone) {
        this.userName = user;
        this.cotation = cotation;
        this.longueur = longueur;
        this.ville = ville;
        this.valideSite = valideSite;
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }


    public String getCotation() {
        return cotation;
    }

    public void setCotation(String cotation) {
        this.cotation = cotation;
    }

    public String getLongueur() {
        return longueur;
    }

    public void setLongueur(String longueur) {
        this.longueur = longueur;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public boolean isValideSite() {
        return valideSite;
    }

    public boolean setValideSite(boolean valideSite) {
        this.valideSite = valideSite;
        return valideSite;
    }
}
