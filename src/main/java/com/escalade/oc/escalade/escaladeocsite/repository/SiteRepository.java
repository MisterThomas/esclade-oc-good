package com.escalade.oc.escalade.escaladeocsite.repository;

import com.escalade.oc.escalade.escaladeocsite.model.Site;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface SiteRepository extends JpaRepository<Site, Long> {

    List<Site> findByUserName(String site);

    @Query("SELECT s FROM Site s Where (?1=''  or s.cotation=?1)  and  (?2='' or s.longueur=?2) and (?3='' or s.ville=?3)")
    List<Site> findSiteByAndCotationAndLongueurAndVille(String cotation, String longueur, String ville);


}
