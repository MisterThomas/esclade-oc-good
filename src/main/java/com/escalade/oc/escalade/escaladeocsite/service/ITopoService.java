package com.escalade.oc.escalade.escaladeocsite.service;

import com.escalade.oc.escalade.escaladeocsite.model.Topo;
import com.escalade.oc.escalade.escaladeocsite.model.User;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface ITopoService {

    List<Topo> getTopoById(String topo);

    Optional<Topo> getTopoById(long id);

    List<Topo> getTopoBySiteId(Long siteId);

    List<Topo> getTopoByUser(User user);
    
    void addTopo(String user, String descriptionTopo, String texteTopo, boolean disponibleTopo,boolean finiTopo, Date targetDateTopo, String adresse, boolean isDone);

    void updateTopo(Topo topo);

    void deleteTopo(long id);

    void saveTopo(Topo topo);
}
