package com.escalade.oc.escalade.escaladeocsite.repository;

import com.escalade.oc.escalade.escaladeocsite.model.Reservation;
import com.escalade.oc.escalade.escaladeocsite.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ReservationRepository extends JpaRepository<Reservation, Long> {
    List<Reservation> findByUserName(String reservation);

    @Query("SELECT r FROM Reservation r where r.topo.id= :topoId")
    List<Reservation> findReservationByTopoId(long topoId);


    List<Reservation> findReservationByUser(User user);

}
