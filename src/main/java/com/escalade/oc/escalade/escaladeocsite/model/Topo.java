package com.escalade.oc.escalade.escaladeocsite.model;


import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

@Entity
@Table(name = "topo", schema = "public")
public class Topo {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;


    private String userName;

    @Column(name = "description_topo")
    @Size(min = 3, message = "Entrer 3 charactere de plus ...")
    private String descriptionTopo;

    @Column(name = "texte_topo")
    @Size(min = 10, message = "Enter plus de 10 characteres...")
    private String texteTopo;

    @Column(name = "adresse")
    private String adresse;


    private Date targetDateTopo = new Date();

    @NotNull
    @Column(nullable = true, name = "disponible_topo")
    private boolean disponibleTopo;


    @NotNull
    @Column(nullable = true, name = "fini_topo")
    private boolean finiTopo;

/*    @ElementCollection(targetClass = Disp
onibleEnum.class, fetch = FetchType.EAGER)
    @Column(name = "topo_choix")
    private List <DisponibleEnum>  disponibleEnums;*/


    public Topo() {
    }


    public Topo(String user, @Size(min = 3, message = "Entrer 3 charactere de plus ...") String descriptionTopo, @Size(min = 10, message = "Enter plus de 10 characteres...") String texteTopo, boolean disponibleTopo,boolean finiTopo, Date targetDateTopo, String adresse, boolean isDone) {
        this.userName = user;
        this.descriptionTopo = descriptionTopo;
        this.texteTopo = texteTopo;
        this.disponibleTopo = disponibleTopo;
        this.finiTopo = finiTopo;
        this.targetDateTopo = targetDateTopo;
        this.adresse = adresse;

    }
/*
    public Topo(String user, @Size(min = 3, message = "Entrer 3 charactere de plus ...") String descriptionTopo, @Size(min = 10, message = "Enter plus de 10 characteres...") String texteTopo,boolean disponibleTopo, Date targetDateTopo,String ville, int codepostal,List <DisponibleEnum> disponibleEnums, boolean isDone) {
        this.userName = user;
        this.descriptionTopo = descriptionTopo;
        this.texteTopo = texteTopo;
        this.disponibleTopo = disponibleTopo;
        this.targetDateTopo = targetDateTopo;
        this.ville =ville;
        this.codepostal=codepostal;
        this.disponibleEnums = disponibleEnums;

    }
*/


    @ManyToOne
    @JoinColumn(name = "site_id", nullable = false)
    private Site site;


    public Site getSite() {
        return site;
    }

    public void setSite(Site site) {
        this.site = site;
    }


    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public String setUserName(String userName) {
        this.userName = userName;
        return userName;
    }

    public String getDescriptionTopo() {
        return descriptionTopo;
    }

    public void setDescriptionTopo(String descriptionTopo) {
        this.descriptionTopo = descriptionTopo;
    }

    public String getTexteTopo() {
        return texteTopo;
    }

    public void setTexteTopo(String texteTopo) {
        this.texteTopo = texteTopo;
    }


    public Date getTargetDateTopo() {
        return targetDateTopo;
    }

    public void setTargetDateTopo(Date targetDateTopo) {
        this.targetDateTopo = targetDateTopo;
    }


    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public boolean isDisponibleTopo() {
        return disponibleTopo;
    }


    public boolean setDisponibleTopo(boolean disponibleTopo) {
        this.disponibleTopo = disponibleTopo;
        return disponibleTopo;
    }


    public boolean isFiniTopo() {
        return finiTopo;
    }

    public boolean setFiniTopo(boolean finiTopo) {
        this.finiTopo = finiTopo;
        return finiTopo;
    }
}
