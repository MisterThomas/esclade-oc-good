package com.escalade.oc.escalade.escaladeocsite.controller;


import com.escalade.oc.escalade.escaladeocsite.model.Reservation;
import com.escalade.oc.escalade.escaladeocsite.model.Site;
import com.escalade.oc.escalade.escaladeocsite.model.Topo;
import com.escalade.oc.escalade.escaladeocsite.model.User;
import com.escalade.oc.escalade.escaladeocsite.repository.ReservationRepository;
import com.escalade.oc.escalade.escaladeocsite.repository.TopoRepository;
import com.escalade.oc.escalade.escaladeocsite.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import javax.validation.Valid;
import java.util.List;

@Controller
public class ReservationController {

    @Autowired
    TopoService topoService;

    @Autowired
    SiteService siteService;


    @Autowired
    UserService userService;

    @Autowired
    ReservationService reservationService;

    @Autowired
    ReservationRepository reservationRepository;


    @Autowired
    TopoRepository topoRepository;


    private String getLoggedInUserName(ModelMap model) {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (principal instanceof UserDetails) {
            return ((UserDetails) principal).getUsername();
        }

        return principal.toString();
    }


    @GetMapping("/reservations")
    public String reservations(ModelMap model) {
        String userName = getLoggedInUserName(model);
        User userReservation = userService.findByEmail(userName);
        List<Reservation> reservationList = reservationService.getReservationByUser(userReservation);
        model.addAttribute("reservations", reservationList);

        return "list-reservation";
    }


    @GetMapping("list-site/{siteId}/list-topo/{topoId}/add-reservation")
    public String showAddReservation(@PathVariable("siteId") long siteId, @PathVariable("topoId") long topoId, ModelMap model, @Valid Reservation reservation) {
        Site site = siteService.getSitesBySite(siteId).get();
        String email = site.getUserName();
        reservation.setUserName(getLoggedInUserName(model));
        User user = userService.findByEmail(email);
        Topo topo = topoService.getTopoById(topoId).get();
        reservation.setUser(user);
        reservation.setTopo(topo);


        if (topo.isFiniTopo() == false && topo.getUserName().equals((getLoggedInUserName(model)))) {
            topo.setFiniTopo(true);
            topo.setDisponibleTopo(false);
            topoService.saveTopo(topo);
            return "redirect:/list-site/{siteId}/list-topo";
        }


        if (topo.isFiniTopo() && topo.isDisponibleTopo() == false) {
            topo.setDisponibleTopo(true);
            reservation.setReservationTopo(false);
            topoService.saveTopo(topo);
            reservationService.saveReservation(reservation);
            return "redirect:/list-site/{siteId}/list-topo";
        }

        return "redirect:/show-site/{siteId}";
    }


    @GetMapping("/reservations/update-reservation/{reservationId}")
    public String showUpdateReservation(@PathVariable("reservationId") long reservationId, ModelMap model, @Valid Topo topo, BindingResult result) {


        Reservation reservation = reservationService.getReservationById(reservationId).get();
        String email = reservation.getUserName();
        reservation.setUserName(getLoggedInUserName(model));

        if (reservation.isReservationTopo() == false) {
            reservation.setAnnulerTopo(false);
            reservation.setReservationTopo(true);
            reservationService.saveReservation(reservation);
            return "redirect:/reservations";
        }
        return "redirect:/reservations";
    }


    @GetMapping("/reservations/delete-reservation/{reservationId}")
    public String showAnnulerReservation(@PathVariable("reservationId") long reservationId, @Valid Reservation reservation, ModelMap model, BindingResult result) {


        reservation = reservationService.getReservationById(reservationId).get();
        Topo topo = reservation.getTopo();
        reservation.setTopo(topo);
        topo.getSite();
        String email = reservation.getUserName();
        reservation.setUserName(getLoggedInUserName(model));

        if (reservation.isAnnulerTopo() == true) {
            return "redirect:/reservations";
        }


        reservation.setAnnulerTopo(true);
        topo.setDisponibleTopo(false);
        reservation.setReservationTopo(false);
        topoService.saveTopo(topo);
        reservationService.saveReservation(reservation);

        return "redirect:/reservations";
    }

}