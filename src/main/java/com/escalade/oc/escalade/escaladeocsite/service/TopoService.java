package com.escalade.oc.escalade.escaladeocsite.service;

import com.escalade.oc.escalade.escaladeocsite.model.Topo;
import com.escalade.oc.escalade.escaladeocsite.model.User;
import com.escalade.oc.escalade.escaladeocsite.repository.TopoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.validation.constraints.Size;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class TopoService implements ITopoService {


    @Autowired
    private TopoRepository topoRepository;


    @Override
    public List<Topo> getTopoById(String topo) {
        return topoRepository.findByUserName(topo);
    }

    @Override
    public Optional<Topo> getTopoById(long id) {
        return topoRepository.findById(id);
    }

    @Override
    public List<Topo> getTopoBySiteId(Long siteId) {
        return topoRepository.findTopoBySiteId(siteId);
    }


    @Override
    public List<Topo> getTopoByUser(User user) {
        return topoRepository.findTopoByUser(user);
    }

    @Override
    public void addTopo(String user, String descriptionTopo, String texteTopo, boolean disponibleTopo,boolean finiTopo, Date targetDateTopo, String adresse, boolean isDone) {
        topoRepository.save(new Topo(user, descriptionTopo, texteTopo, disponibleTopo,finiTopo, targetDateTopo, adresse, isDone));
    }


    @Override
    public void updateTopo(Topo topo) {
        // articleRepository.save(topo);
        topoRepository.save(topo);
    }


    @Override
    public void deleteTopo(long commentaireId) {
        Optional<Topo> topo = topoRepository.findById(commentaireId);
        if (topo.isPresent()) {
            topoRepository.delete(topo.get());
        }
    }

    @Override
    public void saveTopo(Topo topo) {
        topoRepository.save(topo);
    }
}
