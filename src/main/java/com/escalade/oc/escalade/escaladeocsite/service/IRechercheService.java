package com.escalade.oc.escalade.escaladeocsite.service;

import com.escalade.oc.escalade.escaladeocsite.model.Site;

import java.util.List;

public interface IRechercheService {

    List<Site> siteByRecherche(String cotation, String longueur, String ville);

}
