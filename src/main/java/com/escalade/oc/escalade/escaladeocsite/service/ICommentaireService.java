package com.escalade.oc.escalade.escaladeocsite.service;

import com.escalade.oc.escalade.escaladeocsite.model.Commentaire;
import com.escalade.oc.escalade.escaladeocsite.model.User;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public interface ICommentaireService {


    List<Commentaire> getCommentaireById(String commentaire);

    Optional<Commentaire> getCommentaireById(long id);


    List<Commentaire> getCommentaireBySiteId(long siteId);

    List<Commentaire> getCommentaireByUser(String user);


    void updateCommentaire(Commentaire commentaire);

    void addCommentaire(String name, String texteCommentaire, Date targetDate, boolean isDone);

    void deleteCommentaire(long commentaireId);

    void saveCommentaire(Commentaire commentaire);
}
