package com.escalade.oc.escalade.escaladeocsite.repository;


import com.escalade.oc.escalade.escaladeocsite.model.Topo;
import com.escalade.oc.escalade.escaladeocsite.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface TopoRepository extends JpaRepository<Topo, Long> {
    List<Topo> findByUserName(String topo);

    @Query("SELECT t FROM Topo t where t.site.id = :siteId")
    List<Topo> findTopoBySiteId(long siteId);

    List<Topo> findTopoByUser(User user);
}
