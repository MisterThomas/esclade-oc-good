package com.escalade.oc.escalade.escaladeocsite.model;


import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Table(name = "reservation")
public class Reservation {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @NotNull
    @Column(name = "reservation_topo", nullable = false)
    private boolean reservationTopo;


    @NotNull
    @Column(name = "annuler_topo", nullable = false)
    private boolean annulerTopo;


    @Column(name = "username")
    private String userName;


    private Date targetDate;


    @OneToOne
    @JoinColumn(name = "topo_id", nullable = false)
    private Topo topo;

    public Topo getTopo() {
        return topo;
    }

    public void setTopo(Topo topo) {
        this.topo = topo;
    }


    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Reservation() {
        super();
    }


    public Reservation(String user, boolean reservationTopo, boolean annulerTopo, Date targetDate, boolean isDone) {
        this.userName = user;
        this.reservationTopo = reservationTopo;
        this.annulerTopo = annulerTopo;
        this.targetDate = targetDate;
    }


    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    private User user;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getTargetDate() {
        return targetDate;
    }

    public void setTargetDate(Date targetDate) {
        this.targetDate = targetDate;
    }


    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public boolean isReservationTopo() {
        return reservationTopo;
    }


    public boolean setReservationTopo(boolean reservationTopo) {
        this.reservationTopo = reservationTopo;
        return reservationTopo;
    }

    public boolean isAnnulerTopo() {
        return annulerTopo;
    }

    public boolean setAnnulerTopo(boolean annulerTopo) {
        this.annulerTopo = annulerTopo;
        return annulerTopo;
    }

}
