package com.escalade.oc.escalade.escaladeocsite.service;

import com.escalade.oc.escalade.escaladeocsite.model.User;
import com.escalade.oc.escalade.escaladeocsite.repository.UserRepository;


public interface IUserService {
    void save(UserRepository userRepository);

    User findByEmail(String email);
}
