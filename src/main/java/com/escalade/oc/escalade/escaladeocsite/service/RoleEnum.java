package com.escalade.oc.escalade.escaladeocsite.service;

public enum RoleEnum {
    USER,
    ADMINSTRATOR
}
