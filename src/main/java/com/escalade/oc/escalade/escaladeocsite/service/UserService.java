package com.escalade.oc.escalade.escaladeocsite.service;


import com.escalade.oc.escalade.escaladeocsite.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import com.escalade.oc.escalade.escaladeocsite.model.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.*;

@Service("UserService")
public class UserService implements UserDetailsService {

    @Autowired
    private final UserRepository userRepository;


    @Autowired
    private BCryptPasswordEncoder passwordEncoder;


    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        List<User> users = userRepository.findByEmail(email);
        if (users == null || users.isEmpty()) {
            throw new UsernameNotFoundException("No user present with username : " + email);
        } else {
            return users.get(0);
        }
    }

    public void save(User user) {
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        Collection<RoleEnum> collection = new HashSet<RoleEnum>();
        collection.add(RoleEnum.ADMINSTRATOR);
        user.setRoles(collection);
        userRepository.save(user);
    }


    public com.escalade.oc.escalade.escaladeocsite.model.User findByEmail(String email) {

        return userRepository.findByEmail(email).get(0);
    }
}
