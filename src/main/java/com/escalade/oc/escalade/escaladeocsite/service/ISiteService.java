package com.escalade.oc.escalade.escaladeocsite.service;

import com.escalade.oc.escalade.escaladeocsite.model.Site;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface ISiteService {

    List<Site> getSitesBySite(String site);

    Optional<Site> getSitesBySite(long id);

    void updateSite(Site site);

    void addSite(String user, String cotation, String longueur, String ville, boolean valideSite, boolean isDone);

    void deleteSite(long id);

    void saveSite(Site site);
}
