package com.escalade.oc.escalade.escaladeocsite.repository;

import com.escalade.oc.escalade.escaladeocsite.model.Commentaire;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface CommentaireRepository extends JpaRepository<Commentaire, Long> {
    List<Commentaire> findByUserName(String commentaire);

    @Query("SELECT c FROM Commentaire c where c.site.id = :siteId")
    List<Commentaire> findCommentaireBySiteId(long siteId);

    List<Commentaire> findCommentaireByUserName(String user);

}
