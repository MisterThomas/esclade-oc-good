package com.escalade.oc.escalade.escaladeocsite.controller;

import com.escalade.oc.escalade.escaladeocsite.config.WebSecurityConfig;
import com.escalade.oc.escalade.escaladeocsite.model.User;
import com.escalade.oc.escalade.escaladeocsite.repository.UserRepository;
import com.escalade.oc.escalade.escaladeocsite.service.UserService;
import com.escalade.oc.escalade.escaladeocsite.validator.UserValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

@Controller
public class UserController {
    @Autowired
    private UserRepository userRepository;


    @Autowired
    UserService userService;

    @Autowired
    private UserValidator userValidator;


    @Autowired
    private WebSecurityConfig webSecurityConfig;


    @GetMapping("/signup")
    public String registration(Model model) {
        model.addAttribute("userForm", new User());

        return "signup";
    }

    @PostMapping("/signup")
    public String registrationvalid(@ModelAttribute("userForm") User user, BindingResult bindingResult, Model model) {
        userValidator.validate(user, bindingResult);

        if (bindingResult.hasErrors()) {
            return "signup";
        }


        userService.save((User) user);

        return "redirect:/compte";
    }

    @GetMapping("/login")
    public String loginerror(Model model, String error, String logout) {


        return "login";

    }


    @GetMapping("/adminHome")
    public String loginAdminerror(Model model, String error, String logout) {


        return "adminHome";

    }


    @GetMapping("/appLogout")
    public String logout(Model model) {


        return "redirect:/login";

    }


    @GetMapping("/compte")
    public String compte(Model model) {
        return "compte";
    }


    @GetMapping("/acceuil")
    public String root() {
        return "acceuil";
    }

}
