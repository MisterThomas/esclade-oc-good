package com.escalade.oc.escalade.escaladeocsite.controller;


import com.escalade.oc.escalade.escaladeocsite.model.Commentaire;
import com.escalade.oc.escalade.escaladeocsite.model.Site;
import com.escalade.oc.escalade.escaladeocsite.model.User;
import com.escalade.oc.escalade.escaladeocsite.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Controller
public class CommentaireController {

    @Autowired
    CommentaireService commentaireService;

    @Autowired
    SiteService siteService;


    @Autowired
    UserService iUserService;


    @InitBinder
    public void initBinder(WebDataBinder binder) {
        // Date - dd/MM/yyyy
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, false));
    }


    @GetMapping("list-site/{siteId}/list-commentaire")
    public String ShowCommentaires(@PathVariable("siteId") long id, ModelMap model) {
        String name = getLoggedInUserName(model);
        Site site = siteService.getSitesBySite(id).get();
        List listeCommemtaire = commentaireService.getCommentaireBySiteId(id);
        model.addAttribute("commentaires", listeCommemtaire);
        model.addAttribute("site", site);
        return "list-commentaire";
    }


    private String getLoggedInUserName(ModelMap model) {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (principal instanceof UserDetails) {
            return ((UserDetails) principal).getUsername();
        }
        return principal.toString();
    }


    @GetMapping("list-site/{siteId}/add-commentaire")
    public String showAddCommentaireSite(@PathVariable("siteId") long id, ModelMap model) {
        Site site = siteService.getSitesBySite(id).get();
        model.addAttribute("site", site);
        model.addAttribute("commentaire", new Commentaire());
        return "commentaire";
    }

    @GetMapping("list-site/{siteId}/delete-commentaire/{commentaireId}")
    public String deleteCommentaire(@PathVariable("siteId") long siteId, @PathVariable("commentaireId") long commentaireId, @Valid Commentaire commentaire, ModelMap model) {

        Site site = siteService.getSitesBySite(siteId).get();
        User user = iUserService.findByEmail(getLoggedInUserName(model));
        commentaire = commentaireService.getCommentaireById(commentaireId).get();
        commentaire.setSite(site);


        if (user.getRoles().contains(RoleEnum.ADMINSTRATOR)) {
            commentaireService.deleteCommentaire(commentaireId);
            return "redirect:/list-site/{siteId}/list-commentaire";
        }
        return "redirect:/show-site/{siteId}";
    }

    @GetMapping("list-site/{siteId}/update-commentaire/{commentaireId}")
    public String showUpdateCommentaireSite(@PathVariable("siteId") long id, @PathVariable("commentaireId") long commentaireId, ModelMap model) {
        Site site = siteService.getSitesBySite(id).get();
        User user = iUserService.findByEmail(getLoggedInUserName(model));
        Commentaire commentaire = commentaireService.getCommentaireById(commentaireId).get();
        commentaire.setSite(site);
        model.put("site", site);
        model.put("commentaire", commentaire);

        if (user.getEmail().equals(commentaire.getUserName()) || user.getRoles().contains(RoleEnum.ADMINSTRATOR)) {
            return "update-commentaire";
        }
        return "redirect:/show-site/{siteId}";
    }

    @PostMapping("list-site/{siteId}/update-commentaire/{commentaireId}")
    public String updateCommentaire(@PathVariable("siteId") long id, @PathVariable("commentaireId") long commentaireId, ModelMap model, @Valid Commentaire commentaire, BindingResult result) {
        if (result.hasErrors()) {
            return "commentaire";
        }

        Site site = siteService.getSitesBySite(id).get();
        String email = site.getUserName();
        User user = iUserService.findByEmail(email);
        commentaire.setUser(user);
        commentaire.setSite(site);
        commentaire.setId(commentaireId);

        commentaire.setUserName(getLoggedInUserName(model));
        commentaireService.updateCommentaire(commentaire);
        return "redirect:/list-site/{siteId}/list-commentaire";
    }

    @PostMapping("list-site/{siteId}/add-commentaire")
    public String addCommentaire(@PathVariable("siteId") long id, ModelMap model, @Valid Commentaire commentaire, BindingResult result) {

        if (result.hasErrors()) {
            return "commentaire";
        }
        Site site = siteService.getSitesBySite(id).get();
        String email = site.getUserName();
        User user = iUserService.findByEmail(email);
        commentaire.setUser(user);
        commentaire.setSite(site);
        commentaire.setUserName(getLoggedInUserName(model));
        commentaireService.saveCommentaire(commentaire);
        return "redirect:/list-site/{siteId}/list-commentaire";
    }








}
