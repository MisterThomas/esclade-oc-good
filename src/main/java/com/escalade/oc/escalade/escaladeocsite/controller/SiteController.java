package com.escalade.oc.escalade.escaladeocsite.controller;


import com.escalade.oc.escalade.escaladeocsite.model.Reservation;
import com.escalade.oc.escalade.escaladeocsite.model.Site;
import com.escalade.oc.escalade.escaladeocsite.model.Topo;
import com.escalade.oc.escalade.escaladeocsite.model.User;
import com.escalade.oc.escalade.escaladeocsite.repository.CommentaireRepository;
import com.escalade.oc.escalade.escaladeocsite.service.*;
import com.escalade.oc.escalade.escaladeocsite.repository.SiteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.parameters.P;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;


@Controller
public class SiteController {


    @Autowired
    private ISiteService siteService;


    @Autowired
    private ICommentaireService commentaireService;


    @Autowired
    private ITopoService topoService;

    @Autowired
    SiteRepository siteRepository;

    @Autowired
    CommentaireRepository commentaireRepository;

    @Autowired
    RechercheService rechercheService;

    @Autowired
    UserService userService;

    @InitBinder
    public void initBinder(WebDataBinder binder) {

        // Date - dd/MM/yyyy
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, false));
    }


    @GetMapping("/sites")
    public String sites(ModelMap model, @Valid Site site, @RequestParam(value = "cotation", required = false) String cotation, @RequestParam(value = "longueur", required = false) String longueur, @RequestParam(value = "ville", required = false) String ville) {


        model.put("sites", rechercheService.siteByRecherche(cotation, longueur, ville));


        return "sites";
    }


/*    @GetMapping("/recherche-site")
    public String rechercheSites(@RequestParam(value = "cotation", required = false) String cotation , @RequestParam(value = "longueur", required = false) String longueur, @RequestParam(value = "ville", required = false) String ville, @Valid Site site) {

        List<Site> siteByRecherche = rechercheService.siteByRecherche( cotation,longueur, ville);


        if (siteByRecherche.isEmpty()) {
            return "redirect:/sites";
        }
        return "redirect:/sites";
            }*/


    @GetMapping("/list-site")
    public String showSites(ModelMap model) {
        String name = getLoggedInUserName(model);
        model.put("sites", siteService.getSitesBySite(name));


        return "list-site";
    }


    @GetMapping("/show-site/{id}")
    public String showSite(@PathVariable("id") long id, ModelMap model) {
        Site site = siteService.getSitesBySite(id).get();
        List commentaires = commentaireService.getCommentaireBySiteId(id);
        List topos = topoService.getTopoBySiteId(id);
        model.put("site", site);
        model.put("commentaires", commentaires);
        model.put("topos", topos);
        return "show-site";
    }

    private String getLoggedInUserName(ModelMap model) {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (principal instanceof UserDetails) {
            return ((UserDetails) principal).getUsername();
        }

        return principal.toString();
    }

    @GetMapping("/add-site")
    public String showAddSitePage(ModelMap model) {
        model.addAttribute("site", new Site());
        return "site";
    }

    @GetMapping("/delete-site/{id}")
    public String deleteSite(@PathVariable("id") long id, ModelMap model,@Valid Site site, BindingResult result) {


         site = siteService.getSitesBySite(id).get();


        if (result.hasErrors()) {
            return "redirect:/list-site";
        }

        if(site.getTopo().isEmpty() || site.getCommentaire().isEmpty()) {
            siteService.deleteSite(id);
            return "redirect:/list-site";
        }
        return "redirect:/list-site";
    }

    @GetMapping("/update-site/{id}")
    public String showUpdateSitePage(@PathVariable("id") long id, ModelMap model) {
        Site site = siteService.getSitesBySite(id).get();
        model.put("site", site);
        return "update-site";
    }

    @PostMapping("/update-site/{id}")
    public String updateSite(@PathVariable("id") long id, ModelMap model, @Valid Site site, BindingResult result) {

        if (result.hasErrors()) {
            return "site";
        }

        site.setUserName(getLoggedInUserName(model));
        siteService.updateSite(site);
        return "redirect:/list-site";
    }

    @PostMapping("/add-site")
    public String addSite(ModelMap model, @Valid Site site, BindingResult result) {

        if (result.hasErrors()) {
            return "site";
        }

        site.setUserName(getLoggedInUserName(model));
        siteService.saveSite(site);
        return "redirect:/list-site";
    }


    @GetMapping("/sites/update-statutsite/{siteId}")
    public String statutSiteAdmin(@PathVariable("siteId") long id, ModelMap model, @Valid User user, BindingResult result) {

        Site site = siteService.getSitesBySite(id).get();

        site.setUserName(getLoggedInUserName(model));
        String email = site.getUserName();
        user = userService.findByEmail(email);

        if ((!user.getRoles().contains(RoleEnum.ADMINSTRATOR))) {


            return "redirect:/sites";
        }

        site.setValideSite(true);
        siteService.saveSite(site);
        return "redirect:/sites";

    }
}
