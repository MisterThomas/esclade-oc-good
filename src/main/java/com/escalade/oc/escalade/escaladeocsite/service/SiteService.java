package com.escalade.oc.escalade.escaladeocsite.service;

import com.escalade.oc.escalade.escaladeocsite.model.Site;
import com.escalade.oc.escalade.escaladeocsite.repository.SiteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class SiteService implements ISiteService {

    @Autowired
    private SiteRepository siteRepository;

    @Override
    public List<Site> getSitesBySite(String site) {
        return siteRepository.findByUserName(site);
    }

    @Override
    public Optional<Site> getSitesBySite(long id) {
        return siteRepository.findById(id);
    }

    @Override
    public void updateSite(Site site) {
        siteRepository.save(site);
    }

    @Override
    public void addSite(String user, String cotation, String longueur, String ville, boolean valideSite, boolean isDone) {

        siteRepository.save(new Site(user, cotation, longueur, ville, valideSite, isDone));
    }

    @Override
    public void deleteSite(long id) {
        Optional<Site> site = siteRepository.findById(id);
        if (site.isPresent()) {
            siteRepository.delete(site.get());
        }
    }

    @Override
    public void saveSite(Site site) {
        siteRepository.save(site);
    }
}
