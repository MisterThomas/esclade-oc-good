package com.escalade.oc.escalade.escaladeocsite.service;

import com.escalade.oc.escalade.escaladeocsite.model.Reservation;

import com.escalade.oc.escalade.escaladeocsite.model.User;
import com.escalade.oc.escalade.escaladeocsite.repository.ReservationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class ReservationService implements IReservationService {

    @Autowired
    ReservationRepository reservationRepository;

    @Override
    public List<Reservation> getReservationById(String reservation) {
        return reservationRepository.findByUserName(reservation);
    }

    @Override
    public Optional<Reservation> getReservationById(long id) {
        return reservationRepository.findById(id);
    }

    @Override
    public List<Reservation> getReservationByTopoId(Long topoId) {
        return null;
    }

    @Override
    public List<Reservation> getReservationByUser(User user) {
        return reservationRepository.findReservationByUser(user);
    }


    @Override
    public void addReservation(String user, boolean reservationTopo, boolean annulerTopo, Date targetDate, boolean isDone) {
        Reservation newResa = new Reservation();
        newResa.setUser(null);
        newResa.setTargetDate(null);


        reservationRepository.save(new Reservation(user, reservationTopo, annulerTopo, targetDate, isDone));
    }

    @Override
    public void saveReservation(Reservation reservation) {
        reservationRepository.save(reservation);
    }

}
