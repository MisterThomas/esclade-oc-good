package com.escalade.oc.escalade.escaladeocsite.service;

import com.escalade.oc.escalade.escaladeocsite.model.Reservation;
import com.escalade.oc.escalade.escaladeocsite.model.Topo;
import com.escalade.oc.escalade.escaladeocsite.model.User;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface IReservationService {

    List<Reservation> getReservationById(String reservation);

    Optional<Reservation> getReservationById(long id);

    List<Reservation> getReservationByTopoId(Long topoId);

    List<Reservation> getReservationByUser(User user);

    void addReservation(String user, boolean reservationTopo, boolean annulerTopo, Date targetDate, boolean isDone);

    void saveReservation(Reservation reservation);

}
