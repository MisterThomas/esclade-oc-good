package com.escalade.oc.escalade.escaladeocsite.controller;

import com.escalade.oc.escalade.escaladeocsite.model.*;
import com.escalade.oc.escalade.escaladeocsite.repository.TopoRepository;
import com.escalade.oc.escalade.escaladeocsite.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Controller
public class TopoController {

    @Autowired
    private TopoService topoService;

    @Autowired
    private SiteService siteService;

    @Autowired
    UserService UserService;


    @Autowired
    TopoRepository topoRepository;


    @InitBinder
    public void initBinder(WebDataBinder binder) {
        // Date - dd/MM/yyyy
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, false));
    }


    @GetMapping("/topos")
    public String topos(ModelMap model) {
        String userName = getLoggedInUserName(model);
   //     User userTopo = UserService.findByEmail(userName);
     //   List<Topo> topoList = topoRepository.findTopoByUser(userTopo);
    //    List<Topo> listeTopo = topoService.getTopoByUser(userTopo);
        model.addAttribute("topos", topoService.getTopoById(userName));

        return "topos";
    }




    @GetMapping("list-site/{id}/list-topo")
    public String showTopos(@PathVariable("id") long id, ModelMap model) {
        String name = getLoggedInUserName(model);
        Site site = siteService.getSitesBySite(id).get();
        List listeTopo = topoService.getTopoBySiteId(id);

        model.addAttribute("topos", listeTopo);
        model.addAttribute("site", site);
        return "list-topo";
    }

    private String getLoggedInUserName(ModelMap model) {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (principal instanceof UserDetails) {
            return ((UserDetails) principal).getUsername();
        }

        return principal.toString();
    }

    @GetMapping("list-site/{siteId}/add-topo")
    public String showAddTopoSite(@PathVariable("siteId") long siteId, ModelMap model) {
        Site site = siteService.getSitesBySite(siteId).get();
        model.addAttribute("site", site);
        model.addAttribute("topo", new Topo());
        return "topo";
    }


    @GetMapping("topos/delete-topo/{topoId}")
    public String deleteTopoList(@PathVariable("topoId") long topoId, @Valid Reservation reservation, ModelMap model) {

        Topo topo = topoService.getTopoById(topoId).get();
        if (topo.isDisponibleTopo() == true) {
            return "redirect:/topos";
        }

        topoService.deleteTopo(topoId);
        return "redirect:/topos";
    }


    @GetMapping("topos/update-topo/{topoId}")
    public String showUpdateTopoSite(@PathVariable("topoId") long topoId, @Valid Site site, ModelMap model) {


        Topo topo = topoService.getTopoById(topoId).get();
        Site site2 = topo.getSite();
        model.put("site", site2);
        model.put("topo", topo);
        return "update-topo";
    }

    @PostMapping("list-site/{siteId}/update-topo/{topoId}")
    public String updateTopo(@PathVariable("topoId") long topoId, @PathVariable("siteId") long id, @Valid Topo topo, ModelMap model, BindingResult result) {

        if (result.hasErrors()) {
            return "topo";
        }
        Site site = siteService.getSitesBySite(id).get();
        String email = site.getUserName();
        User user = UserService.findByEmail(email);
        topoService.getTopoById(topoId).get();
        topo.setUser(user);
        topo.setSite(site);
        topo.setId(topoId);
        topo.setUserName(getLoggedInUserName(model));
        topoService.updateTopo(topo);
        return "redirect:/list-site/{siteId}/list-topo";
    }


    @PostMapping("list-site/{siteId}/add-topo")
    public String addTopo(@PathVariable("siteId") long id, ModelMap model, @Valid Topo topo, BindingResult result) {

        if (result.hasErrors()) {
            return "topo";
        }

        Site site = siteService.getSitesBySite(id).get();
        String email = site.getUserName();
        User user = UserService.findByEmail(email);
        topo.setUser(user);
        topo.setSite(site);
        topo.setUserName(getLoggedInUserName(model));
        topoService.saveTopo(topo);
        return "redirect:/list-site/{siteId}/list-topo";
    }
}
