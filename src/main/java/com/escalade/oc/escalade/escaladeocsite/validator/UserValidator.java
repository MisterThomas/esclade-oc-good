package com.escalade.oc.escalade.escaladeocsite.validator;

import com.escalade.oc.escalade.escaladeocsite.model.User;
import com.escalade.oc.escalade.escaladeocsite.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class UserValidator implements Validator {


    private IUserService UserService;

    @Override
    public boolean supports(Class<?> aClass) {
        return User.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        User user = (User) o;

    }
}
