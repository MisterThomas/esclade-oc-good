package com.escalade.oc.escalade.escaladeocsite.service;

import com.escalade.oc.escalade.escaladeocsite.model.Commentaire;
import com.escalade.oc.escalade.escaladeocsite.model.Site;
import com.escalade.oc.escalade.escaladeocsite.repository.CommentaireRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class CommentaireService implements ICommentaireService {
    @Autowired
    private CommentaireRepository commentaireRepository;


    @Override
    public List<Commentaire> getCommentaireById(String commentaire) {
        return commentaireRepository.findByUserName(commentaire);
    }

    @Override
    public Optional<Commentaire> getCommentaireById(long id) {
        return commentaireRepository.findById(id);
    }

    @Override
    public List<Commentaire> getCommentaireBySiteId(long siteId) {
        return commentaireRepository.findCommentaireBySiteId(siteId);
    }

    @Override
    public List<Commentaire> getCommentaireByUser(String user) {
        return commentaireRepository.findByUserName(user);
    }

    @Override
    public void updateCommentaire(Commentaire commentaire) {
        commentaireRepository.save(commentaire);
    }

    @Override
    public void addCommentaire(String user, String texteCommentaire, Date targetDate, boolean isDone) {
        commentaireRepository.save(new Commentaire(user, texteCommentaire, targetDate, isDone));
    }

    @Override
    public void deleteCommentaire(long commentaireId) {

        Optional<Commentaire> commentaire = commentaireRepository.findById(commentaireId);
        if (commentaire.isPresent()) {
            commentaireRepository.delete(commentaire.get());
        }
    }

    @Override
    public void saveCommentaire(Commentaire commentaire) {
        commentaireRepository.save(commentaire);
    }


}
