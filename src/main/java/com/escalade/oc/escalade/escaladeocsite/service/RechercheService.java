package com.escalade.oc.escalade.escaladeocsite.service;

import com.escalade.oc.escalade.escaladeocsite.model.Site;
import com.escalade.oc.escalade.escaladeocsite.model.User;
import com.escalade.oc.escalade.escaladeocsite.repository.SiteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RechercheService implements IRechercheService {


    @Autowired
    private SiteRepository siteRepository;


    @Override
    public List<Site> siteByRecherche(String cotation, String longueur, String ville) {

        if (cotation == null && ville == null && longueur == null) {
            return siteRepository.findAll();
        }

        cotation = !"aucune".equalsIgnoreCase(cotation) ? cotation : "";
        longueur = !"aucune".equalsIgnoreCase(longueur) ? longueur : "";
        ville = !"aucune".equalsIgnoreCase(ville) ? ville : "";

        return siteRepository.findSiteByAndCotationAndLongueurAndVille(cotation, longueur, ville);

    }


}
